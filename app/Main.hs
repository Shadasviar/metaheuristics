module Main where

import Optimization
import System.Environment

main :: IO ()
main = do
  args <- getArgs
  let (_:_:n:xs) = read <$> args :: [Double]
  print $ growWorlds
    (mutationCauche (-1e-5, 1e-5))
    (mutationNormal (-0.1, 0.1))
    (<) rosenbrock (round n) xs
