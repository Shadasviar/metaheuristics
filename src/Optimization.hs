module Optimization
  ( rosenbrock
  , rosenbrock3d
  , plot
  , growRand
  , grow
  , growWorlds
  , mutationCauche
  , mutationNormal
  , scale
  ) where

import Graphics.Gnuplot.Simple
import System.Random
import Random

rosenbrock :: Floating a => [a] -> a
rosenbrock xs = foldl step 0 xs'
  where
    step acc (xpre, x) = acc + (100 * ((x - xpre ** 2) ** 2) + (1 - xpre) ** 2)
    xs' = zip xs (tail xs)

rosenbrock3d :: Floating a => a -> a -> a
rosenbrock3d x y = rosenbrock [x,y]

plot :: IO ()
plot = do
  let xs = [-2,-1.99..2::Double]
  let ys = [-1,-0.99..3::Double]
  plotFunc3d [] [] xs ys rosenbrock3d

-- | Optimize function by random grow
growRand :: Random a
  => (b -> b -> Bool) -- Function for optimization (< or >)
  -> ([a] -> b)       -- Function to be optimized
  -> Int              -- Number of iterations
  -> [a]              -- Initial set of x values
  -> (b, [a])         -- The best found value and its xs
growRand op f n xs = foldl step ((f xs), xs) [1..n]
  where
    step (y, xs') i
      | newY `op` y = (newY, newXs)
      | otherwise = (y, xs')
      where
        newY = f newXs
        newXs = take (length xs) $ randoms (mkStdGen i)

-- | Find min or max of given function.
grow :: (Random a, Num a)
        => (a, a)           -- Search step range (min,max)
        -> (b -> b -> Bool) -- Function for optimization (min or max: < or >).
        -> ([a] -> b)       -- Function to be optimized.
        -> Int              -- Number of iterations.
        -> [a]              -- Initial set of x values for optimized function.
        -> (b, [a])         -- The best found value and its xs.
grow rrange op f n xs = foldl step ((f xs), xs) [1..n]
  where
    step (y, xs') i
      | newY `op` y = (newY, newXs)
      | otherwise = (y, xs')
      where
        newY = f newXs
        newXs = zipWith (+) (randomRs rrange (mkStdGen i)) xs'

growWorlds :: (Random a, Num a, Fractional a)
  => (Int -> [a] -> [a])  -- Local mutation operator
  -> (Int -> [a] -> [a])  -- Far mutation operator
  -> (b -> b -> Bool)     -- Function for optimization (< or >)
  -> ([a] -> b)           -- Function to be optimized
  -> Int                  -- Number of iterations
  -> [a]                  -- Initial set of x values
  -> (b, [a])             -- The best found value and its xs
growWorlds local far cmp f n xs = foldl step (f xs, xs) [1..n]
  where
    step x@(_, stepXs) i = foldl best x [(y', xs'), (y'', xs'')]
      where
        y'    = f xs'
        y''   = f xs''
        xs'   = local i stepXs
        xs''  = far i stepXs
    best a@(a', _) b@(b', _)
      | a' `cmp` b' = a
      | otherwise = b

intMax :: Int
intMax = 4294967296

scale :: (Fractional a) => (a, a) -> [Int] -> [a]
scale (down, up) x = map f x
  where
    f x' = (((up - down) * (fromIntegral x')) / (fromIntegral intMax)) + down

mutationNormal :: Fractional a => (a, a) -> Int -> [a] -> [a]
mutationNormal rng seed xs = zipWith (+) xs rnds
  where
    rnds = scale rng
      $ linearRandomList (1103515245 * seed) 12345 intMax (length xs)

mutationCauche :: Fractional a => (a, a) -> Int -> [a] -> [a]
mutationCauche rng seed xs = zipWith (+) xs rnds
  where
    rnds = scale rng
      $ caucheList (1103515245 * seed, 22695477) (12345, 1) intMax (length xs)
