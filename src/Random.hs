module Random
  ( linearRandomList
  , caucheList
  , hist
  , plotHist
  , probs
  , expect
  , expect2
  , sd
  , stats
  , stdLinearRandList
  , stdCauchyList
  ) where

import Graphics.Gnuplot.Simple
import Data.Array

linearRandomList :: Integral a => a -> a -> a -> a -> [a]
linearRandomList a c m n = tail $ scanl step start [1..n]
  where
    step _ xpre = (a * xpre + c) `mod` m
    start = (a + c) `mod` m

stdLinearRandList :: Int -> Int -> [Int]
stdLinearRandList = linearRandomList 1103515245 12345

hist :: [Int] -> [Int]
hist list = elems $ accumArray (+) 0 bnds $ zip list [1,1..]
  where
    bnds = (minimum list, maximum list)

plotHist :: [Int] -> IO ()
plotHist x = plotListStyle [] (defaultStyle{plotType=Boxes}) dat
  where
    dat = zip [0,1..] (hist x) :: [(Int,Int)]

probs :: [Int] -> [Double]
probs list = map (\x -> (fromIntegral (x :: Int)) / (fromIntegral (length h))) h
  where
    h = hist list

expect :: [Int] -> Double
expect list = sum $ zipWith (*) (fromIntegral <$> list) (probs list)

expect2 :: [Int] -> Double
expect2 list = sum $ zipWith (*) ((**2) <$> fromIntegral <$> list) (probs list)

sd :: [Int] -> Double
sd list = sqrt $ (expect2 list) - (expect list)

caucheList :: (Int,Int) -> (Int,Int) -> Int -> Int -> [Int]
caucheList (a1,a2) (c1,c2) m n = round <$> (*(fm/2)) <$> (+1) <$> select <$> zip3 x y x2
  where
    x = genX n
    x2 = tail $ genX (n+1)
    y = (/fm) <$> fromIntegral <$> linearRandomList a2 c2 m n
    fm = fromIntegral m :: Double
    fu x'
      | (-1 <= x') && (x' <= 1) = (2/pi) * (1/(1 + x'**2))
      | otherwise = 0
    genX n' = (+(-1)) <$> (/(fm/2)) <$> fromIntegral <$> linearRandomList a1 c1 m n'
    select (x',y',x2')
      | (0.5 * y') > (fu x' - (2/pi - 0.5)) = x2'
      | otherwise = x'

stdCauchyList :: Int -> Int -> [Int]
stdCauchyList = caucheList (1103515245, 22695477) (12345, 1)

stats :: [Int] -> IO ()
stats x = do
  plotHist x
  print x
  print $ "Expected value = " ++ show (expect x)
  print $ "Standard deviation = " ++ show (sd x)

